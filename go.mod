module gitlab.com/hectorlachambre-ms/shippy-cli-consignment

go 1.14

require (
	github.com/micro/go-micro/v2 v2.2.0
	gitlab.com/hectorlachambre-ms/shippy-service-consignment v0.2.0
	google.golang.org/grpc v1.26.0
)
