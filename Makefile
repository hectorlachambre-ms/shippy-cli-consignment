build:
	GOOS=linux GOARCH=amd64 go build -o shippy-cli-consignment
	docker build -t shippy-cli-consignment .

run:
	docker run --mount type=bind,source=${PWD}/consignment.json,target=/app/consignment.json shippy-cli-consignment